<?php

define('LIG_G3_DIR', '_g3');
define('LIG_G2_DIR', '_g2');

define('LIG_DEBUG', false);

if (true === LIG_DEBUG) {
    function lightning_debug_mode()
    {
        $options = lightning_get_theme_options();
        // $options = get_option( 'lightning_theme_options' );
        // unset( $options['layout'] );
        // update_option( 'lightning_theme_options', $options );
        print '<pre style="text-align:left">';
        print_r($options);
        print '</pre>';
    }
    add_action('lightning_site_header_after', 'lightning_debug_mode');
}

function lightning_is_g3()
{

    $return = true;
    $g      = get_option('lightning_theme_generation');
    if ('g3' === $g) {
        $return = true;
    } elseif ('g2' === $g) {
        $return = false;
    } else {
        $skin = get_option('lightning_design_skin');
        $options = get_option('lightning_theme_options');
        if ('origin2' === $skin) {
            $return = false;
            update_option('lightning_theme_generation', 'g2');
        } elseif ('origin3' === $skin) {
            $return = true;
            update_option('lightning_theme_generation', 'g3');
        } elseif (get_option('fresh_site') || !$options) {
            // 新規サイトでオプション非保存ならまぁG3っしょ
            $return = true;
            update_option('lightning_theme_generation', 'g3');
        } else {
            // これ以外は従来ユーザーの可能性が高いのでG2
            $return = false;
            update_option('lightning_theme_generation', 'g2');
        }
    }
    return apply_filters('lightning_is_g3', $return);
}

require dirname(__FILE__) . '/inc/class-ltg-template-redirect.php';

/**
 * 最終的に各Gディレクトリに移動
 */
if (!function_exists('lightning_get_template_part')) {
    function lightning_get_template_part($slug, $name = null, $args = array())
    {

        if (lightning_is_g3()) {
            $g_dir = '_g3';
        } else {
            $g_dir = '_g2';
        }

        /**
         * 読み込み優先度
         *
         * 1.child g階層 nameあり
         * 2.child 直下 nameあり
         * 3.parent g階層 nameあり
         *
         * 4.child g階層 nameなし
         * 5.child 直下 nameなし
         * 6.parent g階層 nameなし
         */

        /* Almost the same as the core */
        $template_path_array = array();
        $name                = (string) $name;

        // Child theme G directory
        if (preg_match('/^' . $g_dir . '/', $slug)) {
            // 1. g階層がもともと含まれている場合
            if ('' !== $name) {
                $template_path_array[] = get_stylesheet_directory() . "/{$slug}-{$name}.php";
            }
        } else {
            // g階層が含まれていない場合

            // 1. g階層付きのファイルパス
            if ('' !== $name) {
                $template_path_array[] = get_stylesheet_directory() . '/' . $g_dir . "/{$slug}-{$name}.php";
            }
            // 2. 直下のファイルパス
            if ('' !== $name) {
                $template_path_array[] = get_stylesheet_directory() . "/{$slug}-{$name}.php";
            }
        }

        if (preg_match('/^' . $g_dir . '/', $slug)) {
            // 3. g階層がもともと含まれている場合
            if ('' !== $name) {
                $template_path_array[] = get_template_directory() . "/{$slug}-{$name}.php";
            }
        } else {
            // 3. g階層がもともと含まれていない場合
            if ('' !== $name) {
                $template_path_array[] = get_template_directory() . '/' . $g_dir . "/{$slug}-{$name}.php";
            }
        }

        // Child theme G directory
        if (preg_match('/^' . $g_dir . '/', $slug)) {
            // 4. g階層がもともと含まれている場合
            $template_path_array[] = get_stylesheet_directory() . "/{$slug}.php";
        } else {
            // g階層が含まれていない場合
            // 4. g階層付きのファイルパス
            $template_path_array[] = get_stylesheet_directory() . '/' . $g_dir . "/{$slug}.php";
            // 5. 直下のファイルパス
            $template_path_array[] = get_stylesheet_directory() . "/{$slug}.php";
        }

        if (preg_match('/^' . $g_dir . '/', $slug)) {
            // g階層がもともと含まれている場合
            // 6. 親のg階層
            $template_path_array[] = get_template_directory() . "/{$slug}.php";
        } else {
            // 6. 親のg階層
            $template_path_array[] = get_template_directory() . '/' . $g_dir . "/{$slug}.php";
        }

        foreach ((array) $template_path_array as $template_path) {
            if (file_exists($template_path)) {
                $require_once = false;
                load_template($template_path, $require_once);
                break;
            }
        }
    }
}

if (lightning_is_g3()) {
    require dirname(__FILE__) . '/' . LIG_G3_DIR . '/functions.php';
} else {
    require dirname(__FILE__) . '/' . LIG_G2_DIR . '/functions.php';
}

require dirname(__FILE__) . '/inc/customize-basic.php';
require dirname(__FILE__) . '/inc/tgm-plugin-activation/tgm-config.php';
require dirname(__FILE__) . '/inc/vk-old-options-notice/vk-old-options-notice-config.php';
require dirname(__FILE__) . '/inc/functions-compatible.php';
require dirname(__FILE__) . '/inc/font-awesome/font-awesome-config.php';
require dirname(__FILE__) . '/inc/old-page-template.php';

/**
 * 世代切り替えした時に同時にスキンも変更する処理
 *
 * 世代は lightning_theme_generation で管理している。
 *
 *      generetionに変更がある場合
 *          今の世代でのスキン名を lightning_theme_options の配列の中に格納しておく
 *          lightning_theme_option の中に格納されている新しい世代のスキンを取得
 *          スキンをアップデートする *
 */

function lightning_change_generation($old_value, $value, $option)
{
    // 世代変更がある場合
    if ($value !== $old_value) {

        // 現状のスキンを取得
        $current_skin = get_option('lightning_design_skin');

        if ($current_skin) {
            // オプションを取得
            $options = get_option('lightning_theme_options');
            if (!$options || !is_array($options)) {
                $options = array();
            }
            $options['previous_skin_' . $old_value] = $current_skin;
            // 既存のスキンをオプションに保存
            update_option('lightning_theme_options', $options);
        }

        // 前のスキンが保存されている場合
        if (!empty($options['previous_skin_' . $value])) {
            $new_skin = esc_attr($options['previous_skin_' . $value]);

            // 前のスキンが保存されていない場合
        } else {
            if ('g3' === $value) {
                $new_skin = 'origin3';
            } else {
                $new_skin = 'origin2';
            }
        }
        update_option('lightning_design_skin', $new_skin);
    }
}
add_action('update_option_lightning_theme_generation', 'lightning_change_generation', 10, 3);

//======================================================================
//ここからはLightningではなく、Labu独自のカスタマイズです
//======================================================================

//-----------------------------------------------------
// スラッグ名を取得する関数
//-----------------------------------------------------
function getSlug()
{
    $wp_query =  $GLOBALS['wp_the_query']->get_queried_object();
    $slug = '';
    if (is_front_page()) {
        // トップページ
        $slug = "home";
    } elseif (is_home()) {
        // ブログインデックス
        $slug = "column";
    } elseif (is_category()) {
        // 投稿カテゴリ
        $cat = get_queried_object();
        $slug = $cat->slug;
        //$slug = str_replace('_', ' ', $wp_query->queried_object->slug);
        //$slug = "column";
    } elseif (is_post_type_archive()) {
        // 投稿アーカイブ
        $slug = str_replace('_', ' ', $wp_query->queried_object->name);
        //$slug = "column";
    } elseif (is_author()) {
        // 投稿者別アーカイブ
        //$slug = str_replace('_', ' ', get_queried_object()->data->user_nicename);
        $slug = "column";
    } elseif (is_archive()) {
        // タクソノミーアーカイブ
        $slug = str_replace('_', ' ', $term);
        //$slug = "column";
    } elseif (is_page()) {
        // 固定ページ
        global $post;
        if ($post->post_parent) {
            $post_data = get_post($post->post_parent);
            $slug = $post_data->post_name;
        } else {
            $slug = str_replace('-', ' ', attribute_escape($wp_query->post_name));
        }
    } elseif (is_singular()) {
        // シングル
        //$slug = str_replace('-', ' ', attribute_escape($wp_query->post_category));
        $category = get_the_category();
        $cat = $category[0];
        $slug = $cat->slug; // カテゴリースラッグ
        //$slug = "column";
    }
    return esc_attr($slug);
}

//-----------------------------------------------------
// カテゴリ一覧を描画する関数
//-----------------------------------------------------
function writeCategoryList($slug)
{
    $arrayCategory = array('column', 'create', 'webmarketing', 'rpa', 'others');
    $key = in_array($slug, $arrayCategory);
    if ($key) {
        $categories  = get_terms("category", "fields=all&get=all");
        $result = '<div class="border-bottom pt-3 pb-3 mb-3"><div class="container"><nav class="nav"><span class="nav-link font-weight-bold">カテゴリ：</span>';
        foreach ($categories as $category) {
            $category_link = get_category_link($category->term_id);
            $category_name = $category->name;
            if ($category === end($categories)) {
                $result .= '<a href="' . $category_link . '" class="nav-link pl-3 pr-3 ">' . $category_name . '</a>';
            } else {
                $result .= '<a href="' . $category_link . '" class="nav-link border-right pl-3 pr-3">' . $category_name . '</a>';
            }
        }
        $result .= '</nav></div></div>';
    }
    return $result;
}

//-----------------------------------------------------
// HTMLソースの整形
//-----------------------------------------------------
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles', 10);
remove_action('wp_head', 'wp_generator');
add_filter('lightning_is_page_header', function ($return) {
    return false;
});
//-----------------------------------------------------
// ページヘッダーをパンくずの後ろに出力
//-----------------------------------------------------
add_action('lightning_breadcrumb_after', function () {
    global $post;
    if (empty($post->_lightning_design_setting['hidden_page_header'])) {
        get_template_part('template-parts/page-header');
    }
});
//-----------------------------------------------------
// <head>内に/asset/js/common.jsを追加
//-----------------------------------------------------
function addJs()
{
    wp_enqueue_script('base', '/asset/js/common.js', array('jquery'), '3.5.1', true);
}
add_action('wp_enqueue_scripts', 'addJs');
//-----------------------------------------------------
// <head>内にWEBFONTを追加
//-----------------------------------------------------
function addWebFont()
{
    echo <<< EOM
<style>
@import url('https://fonts.googleapis.com/css2?family=M+PLUS+1p:wght@500&family=M+PLUS+Rounded+1c&display=swap');
</style> 
EOM;
}
add_action('wp_enqueue_scripts', 'addWebFont');
//-----------------------------------------------------
// <head>内にgoogleアナリティクスを追加
//-----------------------------------------------------
function addGtag()
{
    echo <<< EOM
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-XVYM7W7RM0"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'G-XVYM7W7RM0');
</script>
EOM;
}
add_action('wp_head', 'addGtag');

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles',20 );
function theme_enqueue_styles() {
wp_dequeue_style( 'lightning-theme-style' );
$s_dire_uri = get_stylesheet_directory_uri();
$date_query = date("ymdGis", filemtime( get_stylesheet_directory() . '/style.css'));
wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css',
array( 'lightning-design-style' ), $date_query);
}
