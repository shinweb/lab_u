<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link https://ja.wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define( 'DB_NAME', 'lab_u' );

/** MySQL データベースのユーザー名 */
define( 'DB_USER', 'root' );

/** MySQL データベースのパスワード */
define( 'DB_PASSWORD', '' );

/** MySQL のホスト名 */
define( 'DB_HOST', 'localhost' );

/** データベースのテーブルを作成する際のデータベースの文字セット */
define( 'DB_CHARSET', 'utf8mb4' );

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define( 'DB_COLLATE', '' );

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '{9@2xGIFMjN+ot)x~tXh/19o2*fkTu|3*q a;$;$r,SsHpB[3$lL}|S<4M{ZOFy`' );
define( 'SECURE_AUTH_KEY',  'xUpGMmFxC6,3@D{C8.:&H3DicPP6O_u]<|b)>gG,v }s83w}wELAXPgJm3?JYd[m' );
define( 'LOGGED_IN_KEY',    '=NX:[y}*XD6NH!d8g#)7/M#X@<t/:E0G*~zu]ZBceTL?:xJsfmPQ/{x)M|eYh$D,' );
define( 'NONCE_KEY',        '#*^*f#|TqD/b81d2G,ejV$s!U,>;m}{)y**|sf[c4e1[x2;7P6VxkT<pj9]rrZ@@' );
define( 'AUTH_SALT',        'EZiO{i&o!XTBe{{L_wdiCSAf`AT>*NVpMY W|MLd0Y#C8SF&j;4&BrNr)Z+za)ru' );
define( 'SECURE_AUTH_SALT', '4p yjx<K/A%59y_^,9!J[xcS!YL:J;<+S5HIsy<(;D5r`f:|HL6/8T:5;$-w0l%w' );
define( 'LOGGED_IN_SALT',   'LSkyT%sJg+R:1ZrOB&Ki=h+uZ.syL,Jen.56f*(FQI`c+Z+R-Xu]2=rhCg5$G}m0' );
define( 'NONCE_SALT',       ';6P`7LgTCn|v@Ye!-aVLk9;uG2);wHlZ6DbX@l:pO0hlPX.s(V&[a%D/4-}pHC4n' );

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数についてはドキュメンテーションをご覧ください。
 *
 * @link https://ja.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* 編集が必要なのはここまでです ! WordPress でのパブリッシングをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
