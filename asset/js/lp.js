jQuery(function() {
    const firstView = function(scrolltop) {
        let windowHeight = 200;
        jQuery('[data-firstview]').each(function() {
            if (scrolltop < windowHeight) {
                return $(this).addClass("is-firstview");
            } else {
                return $(this).removeClass("is-firstview");
            }
        });
    }

    /*---------------------------------
  
    画面内に表示された要素をアニメーションさせる
  
    ---------------------------------*/
    const animation = function(scrolltop) {
        jQuery('[data-animation]').each(function() {
            let $this = jQuery(this);
            let elemPos = $this.offset().top;
            let windowHeight = jQuery(window).height();
            if (scrolltop > elemPos - windowHeight + 200 && scrolltop < elemPos + windowHeight + 200) {
                if ($this.data("animation") == "parent") {
                    $this.find('[data-animation="child"]').each(function() {
                        jQuery(this).addClass('is-trans-active');
                    });
                } else {
                    jQuery(this).addClass('is-trans-active');
                }
            }
        });
    }

    const trans = function(scrolltop) {
        jQuery('.trans').each(function() {
            let $this = jQuery(this);
            let elemPos = $this.offset().top;
            let windowHeight = jQuery(window).height();
            if (scrolltop > elemPos - windowHeight + 200 && scrolltop < elemPos + windowHeight + 200) {
                return $this.addClass('is-trans-active');
            }
        });
    }

    /*---------------------------------
       
    アコーディオン処理
       
     ---------------------------------*/
    const accordion = function() {
        jQuery('.accordion_btn').on('click', function() {
            let target = jQuery(this).data('target');
            let activeClass = 'is-active';
            jQuery(target).slideToggle();
            if (jQuery(this).hasClass(activeClass)) {
                jQuery(this).removeClass(activeClass);
            } else {
                jQuery(this).addClass(activeClass);
            }
        });
    }

    /*---------------------------------
          
       トップページ：最新記事一件を表示
          
        ---------------------------------*/
    const home_news = function() {
        jQuery("#vkexunit_post_list-2 li:first-child .postList_date").clone().appendTo('#sec1_news');
        jQuery("#vkexunit_post_list-2 li:first-child .entry-title").clone().appendTo('#sec1_news');
    }

    /*---------------------------------
          
       トップページ：キービジュアルの処理
          
        ---------------------------------*/
    const home_keyvisual = function() {
        jQuery(".home_keyvisual img").addClass('is-active');
    }

    /*---------------------------------
     
    グローバル変数
     
    ---------------------------------*/
    let scrolltop = jQuery(window).scrollTop();

    /*---------------------------------
     
    ページ読み込み直後に実行
     
    ---------------------------------*/
    firstView(scrolltop);
    animation(scrolltop);
    home_news();
    home_keyvisual();
    accordion();

    /*---------------------------------
     
    スクロール時に実行
     
    ---------------------------------*/
    let timer = false;
    jQuery(window).scroll(function() {
        scrolltop = jQuery(window).scrollTop();
        animation(scrolltop);
        if (timer !== false) {
            clearTimeout(timer);
        }
        timer = setTimeout(function() {
            firstView(scrolltop);
        }, 200);
    });
});