<?php
include '../static-app/config.php'; //サイト全体を総括する設定ファイル
include '../static-app/page.php'; //ページ全体を総括する設定ファイル
include '../static-app/library/mail_post.php'; //フォームに関する便利ファイル
page::$title = "内容確認";
include '../static-tmpl/header.php'; //ヘッダーのテンプレート読み込み
?>

<header id="top">
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top shadow-sm border-bottom p-0">
        <div class="container p-2 p-md-0">
            <h1 class="m-0 p-0 h-65px"><img src="/asset/img/rpa/logo_title.png" alt="<?php echo page::$title; ?>" class="img-fix" /></h1>
        </div>
    </nav>
</header>

<main class="mh-800px">
    <article class="container pt-5">
        <h1 class="pt-5 text-center"><?php echo page::$title; ?></h1>
        <?php echo $body; ?>
    </article>
</main>

<?php include_once '../static-tmpl/footer.php'; ?>
</body>

</html>