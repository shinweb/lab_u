<?php
include '../static-app/config.php'; //サイト全体を総括する設定ファイル
include '../static-app/page.php'; //ページ全体を総括する設定ファイル
page::$title = "申込み受付完了";
include '../static-tmpl/header.php'; //ヘッダーのテンプレート読み込み
?>

<header id="top">
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top shadow-sm border-bottom p-0">
        <div class="container p-2 p-md-0">
            <h1 class="m-0 p-0 h-65px"><img src="/asset/img/rpa/logo_title.png" alt="<?php echo page::$title; ?>" class="img-fix" /></h1>
        </div>
    </nav>
</header>

<main class="mh-800px">
    <article class="container pt-5">
        <h1 class="pt-5 text-center"><?php echo page::$title; ?></h1>
        <p class="text-center">お申込みありがとうございました。</p>
        <p class="text-center">お申し込み後に確認メール・確認のお電話が届かない場合<br>入力内容に誤りがある可能性があります。</p>
        <p class="text-center">下記へお問い合わせください<br>TEL&nbsp;:&nbsp;<span class="font-xlarge font-weight-bold text-pink"><?php echo config::companyTel; ?></span></p>
        <div>
            <a href="./" class="btn btn-lg border mx-auto d-block w-300px"><i class="fa fa-chevron-circle-right mr-2 text-ct" aria-hidden="true"></i>トップページへ</a>
        </div>
    </article>
</main>

<?php include_once '../static-tmpl/footer.php'; ?>