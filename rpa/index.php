<?php
include '../static-app/config.php'; //サイト全体を総括する設定ファイル
include '../static-app/page.php'; //ページ全体を総括する設定ファイル
include '../static-app/view/form.php'; //フォームに関する便利ファイル
page::$title = "【RPA・DX】次世代型人材育成研修サービス";
include '../static-tmpl/header.php'; //ヘッダーのテンプレート読み込み
?>

<header id="top">
    <div class="navbar-light bg-white fixed-top shadow-sm border-bottom">
        <div class="w-100 w-xl-1140px mx-auto">
            <nav class="nav navbar navbar-expand-lg p-0">
                <h1 class="m-0 p-0 h-65px"><img src="/asset/img/rpa/logo_title.png" alt="<?php echo page::$title; ?>" class="img-fix" /></h1>
                <button class="navbar-toggler mr-1" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup">
                    <ul class="navbar-nav font-notosans">
                        <li class="nav-item border-top-lg"><a class="nav-link text-dark p-lg-0" href="#sec1"><span class="line-height-lg-65px pl-3 pr-3">なぜ研修が必要か</span></a></li>
                        <li class="nav-item border-top-lg"><a class="nav-link text-dark p-lg-0" href="#sec2"><span class="line-height-lg-65px pl-3 pr-3">業務改革の課題</span></a></li>
                        <li class="nav-item border-top-lg"><a class="nav-link text-dark p-lg-0" href="#sec3"><span class="line-height-lg-65px pl-3 pr-3">サービス紹介</span></a></li>
                        <li class="nav-item border-top-lg"><a class="nav-link text-dark p-lg-0" href="#sec4"><span class="line-height-lg-65px pl-3 pr-3">講師紹介</span></a></li>
                        <li class="nav-item border-top-lg"><a class="nav-link text-white bg-ct p-lg-0" href="#form"><span class="line-height-lg-65px pl-3 pr-3 pl-lg-4 pr-lg-4">お申し込み</span></a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>

<main>
    <header class="jumbotron-fluid m-0 bg-kv" data-keyvisual>
        <div class="container font-notosans" style="padding-top:90px; min-height:580px;">
            <div class="trans trans-wait-6 trans-right_xsmall pb-2" data-animation>
                <strong class="font-large font-md-xxlarge line-height-1-2"><span class="">DX</span>をする上で<br><span class="">RPA</span>は知っておきたい技術です</strong>
            </div>
            <p class="lead trans trans-wait-9 pb-1" data-animation>これからのRPAの波をキャッチできるよう、我々の研修サービスでサポートします。</p>
            <div class="trans trans-wait-12 trans-bottom_xsmall trans-duration-slow" data-animation>
                <a href="#form" class="btn-img"><img src="/asset/img/rpa/pic_1.png" alt="お申し込みはこちらから" class="w-200px w-lg-auto"></a>
            </div>
            <div class="d-none"><img src="/asset/img/rpa/kv_column.png" alt="背景画像"></div>
        </div>
    </header>

    <section class="p-3 bg-dark text-white">
        <div class="container font-notosans" id="sec1">
            RPA（Robotic Process Automation）とは、「認知技術（ルールエンジン・機械学習）を活用した、主にホワイトカラー業務の効率化・自動化の取組み」です。人間の補完として業務を遂行できることから、仮想知的労働者（Digital Labor）とも言われています。
        </div>
        <footer id="sec1"></footer>
    </section>

    <section class="mh-400px pt-3 pb-3">
        <div class="container">
            <header class="p-3 text-center">
                <h2 class="font-notosans text-ct font-weight-bold font-large font-md-xlarge pb-2">なぜRPA/DX<br>次世代型企業研修が必要か</h2>
                <p class="font-xmiddle m-0">これからのRPA導入企業拡大に伴いRPA技術者の養成が求められていますが、<b>圧倒的に人材が不足</b>しています</p>
            </header>
            <div class="row">
                <div class="col-6 col-lg-3 trans trans-bottom_xsmall" data-animation>
                    <div class="text-center mb-2 w-75 mx-auto d-block"><img src="/asset/img/rpa/sec1_pic_1.png" class="img-fluid p-2"></div>
                    <p>VUCAの時代へ変化し、予測困難な未来に対して迅速な業務改革が求められる</p>
                </div>
                <div class="col-6 col-lg-3 trans trans-wait-1 trans-bottom_xsmall" data-animation>
                    <div class="text-center mb-2 w-75 mx-auto d-block"><img src="/asset/img/rpa/sec1_pic_2.png" class="img-fluid p-2"></div>
                    <p>DX推進として、従来のシステム開発スピードより高速に業務改善が可能なソリューションであるRPAが注目される</p>
                </div>
                <div class="col-6 col-lg-3 trans trans-wait-2 trans-bottom_xsmall" data-animation>
                    <div class="text-center mb-2 w-75 mx-auto d-block"><img src="/asset/img/rpa/sec1_pic_3.png" class="img-fluid p-2"></div>
                    <p>便利であるRPAが正しく導入されていないため、不便な存在になっているケースが多い</p>
                </div>
                <div class="col-6 col-lg-3 trans trans-wait-3 trans-bottom_xsmall" data-animation>
                    <div class="text-center mb-2 w-75 mx-auto d-block"><img src="/asset/img/rpa/sec1_pic_4.png" class="img-fluid p-2"></div>
                    <p>正しく使われるよう知見者がコンサルティングする必要がある</p>
                </div>
            </div>
            <footer id="sec2"></footer>
        </div>
    </section>
    <section class="mh-400px bg-lgray pt-5 pb-5">
        <div class="container">
            <header class="pb-3">
                <h2 class="font-notosans text-center text-ct text-ct font-weight-bold font-large font-md-xlarge pb-1">日本企業を取り巻く<br>RPAでの<br class="d-md-none">業務改革の課題</h2>
                <p class="text-center font-xmiddle m-0">同様の失敗をしないために正しいスキルが必要</p>
            </header>
            <div class="bg-white mb-3 border">
                <header class="pl-4 pt-4 pb-3 border-bottom">
                    <h3 class="dec-left_border">大手企業でのRPA導入の失敗例</h3>
                </header>
                <div class="p-4">
                    <div class="row justify-content-center pb-4 pt-3">
                        <div class="col-6 col-md-3 pb-4 trans" data-animation>
                            <div class="text-center font-xxlarge text-black-50 mb-3">
                                <img src="/asset/img/rpa/sec2_pic_1.png" class="img-fluid">
                            </div>
                            導入したが<b class="text-pink">効果が出ない/実感できない</b>
                        </div>
                        <div class="col-6 col-md-3 pb-4 trans trans-wait-1" data-animation>
                            <div class="text-center font-xxlarge text-black-50 mb-3">
                                <img src="/asset/img/rpa/sec2_pic_2.png" class="img-fluid">
                            </div>
                            メンテナンスが頻発し導入前より<b class="text-pink">業務時間が増える</b>
                        </div>
                        <div class="col-6 col-md-3 pb-4 trans trans-wait-2" data-animation>
                            <div class="text-center font-xxlarge text-black-50 mb-3">
                                <img src="/asset/img/rpa/sec2_pic_3.png" class="img-fluid">
                            </div>
                            RPAが<b class="text-pink">停止した際の対応がわからない</b>（業務のブラックボックス化）
                        </div>
                        <div class="col-6 col-md-3 pb-4 trans trans-wait-3" data-animation>
                            <div class="text-center font-xxlarge text-black-50 mb-3">
                                <img src="/asset/img/rpa/sec2_pic_4.png" class="img-fluid">
                            </div>
                            RPA人材がおらず、ベンダー任せで<b class="text-pink">ランニングコストが高い</b>
                        </div>
                        <div class="col-6 col-md-3 pb-4 trans" data-animation>
                            <div class="text-center font-xxlarge text-black-50 mb-3">
                                <img src="/asset/img/rpa/sec2_pic_5.png" class="img-fluid">
                            </div>
                            どの部署でどんなRPAが<b class="text-pink">稼働しているか状況がわからない</b>
                        </div>
                        <div class="col-6 col-md-3 pb-4 trans trans-wait-2" data-animation>
                            <div class="text-center font-xxlarge text-black-50 mb-3">
                                <img src="/asset/img/rpa/sec2_pic_6.png" class="img-fluid">
                            </div>
                            関連会社へ展開したいが、<b class="text-pink">統制が困難</b>
                        </div>
                        <div class="col-6 col-md-3 pb-4 trans trans-wait-3" data-animation>
                            <div class="text-center font-xxlarge text-black-50 mb-3">
                                <img src="/asset/img/rpa/sec2_pic_7.png" class="img-fluid">
                            </div>
                            ロボットが行った業務の時系列・業務詳細が<b class="text-pink">不明瞭で内部監査で指摘</b>される
                        </div>
                    </div>
                    <div class="row align-items-center pb-4 justify-content-center" data-animation="parent">
                        <div class="col-12 col-lg-5">
                            <div class="bg-lgray p-3 h-100 trans trans-wait-3" data-animation="child">
                                <div class="text-center pb-2 font-xxmiddle">
                                    <i class="fa fa-question-circle mr-1 font-large text-ct" aria-hidden="true"></i>
                                    <span class="font-weight-bold font-notosans">なぜ失敗したか</span>
                                </div>
                                <div class="text-center font-xmiddle line-height-1-6">業務改革をおざなりに、<br>導入安価なデスクトップ型を利用</div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-1 text-center trans trans-wait-4 trans-left_small" data-animation="child">
                            <div class="pt-3 pb-3">
                                <i class="fa fa-chevron-right text-gray d-none d-lg-block font-large" aria-hidden="true"></i>
                                <i class="fa fa-chevron-down text-gray d-lg-none font-large" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="col-12 col-lg-5 trans trans-wait-9" data-animation="child">
                            <div class="border border-ct border-bold p-3 h-100">
                                <div class="text-center pb-2 font-xxmiddle">
                                    <i class="fa fa-exclamation-circle mr-1 font-large text-ct d-block d-lg-inline" aria-hidden="true"></i>
                                    <span class="font-weight-bold font-notosans">どうリカバリーしていくか</span>
                                </div>
                                <div class="text-center font-xmiddle line-height-1-6">業務改革とセットでRPA導入し、<br>サーバー型への移行を推進</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="sec2-2" class="bg-white border">
                <header class="pl-4 pt-4 pb-3 border-bottom">
                    <h3 class="dec-left_border">RPAの導入動向</h3>
                </header>
                <div class="p-4">
                    <p class="text-right"><span style="color:#f7dae7;">■</span>2018年6月／<span style="color:#edb0cb;">■</span>2019年1月／<span style="color:#e283ac;">■</span>2019年11月</p>
                    <div class="row pt-2">
                        <div class="col-12 col-lg-4 pb-2">
                            <figure>
                                <figcaption class="text-center"><i class="fa fa-building text-gray mr-2 font-xxmiddle" aria-hidden="true"></i>全体</figcaption>
                                <canvas id="chart1" class="w-75 w-md-100 h-300px d-block mx-auto"></canvas>
                            </figure>
                        </div>
                        <div class="col-12 col-lg-4 pb-2">
                            <figure>
                                <figcaption class="text-center"><i class="fa fa-building text-gray mr-2 font-xxmiddle" aria-hidden="true"></i>大手</figcaption>
                                <canvas id="chart2" class="w-75 w-md-100 h-300px d-block mx-auto"></canvas>
                            </figure>
                        </div>
                        <div class="col-12 col-lg-4 pb-2">
                            <figure>
                                <figcaption class="text-center"><i class="fa fa-building text-gray mr-2 font-xxmiddle" aria-hidden="true"></i>中堅・中小企業</figcaption>
                                <canvas id="chart3" class="w-75 w-md-100 h-300px d-block mx-auto"></canvas>
                            </figure>
                        </div>
                    </div>
                    <div class="text-right pb-3"><small>※株式会社MM総研「RPA国内利用動向調査２０２０」より抜粋</small></div>
                    <div class="pt-3">
                        <p class="text-center p-0 mb-2"><span class="font-xxmiddle font-weight-bold">大企業が失敗した事例がそのまま中小企業へ</span><br>（※中小企業向けに低コストで導入ができるデスクトップ型が更に流行する恐れがある）</p>
                        <p class="text-center"><i class="fa fa-chevron-down text-gray font-large" aria-hidden="true"></i></p>
                        <p class="text-center font-xxmiddle font-md-large font-notosans text-ct font-weight-bold">自社の課題を正確に捉え、業務のあるべき姿を描ける人材、<br>RPAを正しく活用できる人材が必要</p>
                    </div>
                </div>
            </div>
        </div>

        <footer id="sec3"></footer>
        </div>
    </section>
    <section class="mh-400px p-2 p-md-5 bg-lblue">
        <div class="container">
            <header class="p-3">
                <h2 class="font-notosans text-center text-ct font-weight-bold font-large font-md-xlarge pb-1">次世代型企業育成<br class="d-md-none">サービスのご紹介</h2>
                <p class="text-center">研修サービスではスキルに合わせた多段階研修を行っています</p>
            </header>
            <p class="text-center bg-ct text-light d-block pt-2 pb-2 mb-3 font-large line-height-1 letter-spacing-1">研修プラン</p>
            <div class="row">
                <div class="col-12 col-lg-4 pb-3">
                    <div class="border h-100 rounded bg-white">
                        <section>
                            <h3 class="text-center pt-4 pb-3 text-ct font-weight-bold">スタートアップ</h3>
                            <div class="bg-lgray p-3">
                                期間：4時間<br>
                                料金：<span class=" font-xxmiddle font-weight-bold mr-1">20</span>万円／5人
                            </div>
                            <section class="pt-4 pb-3 pl-2 pr-2" data-height="row1">
                                <h4 class="pl-3 font-middle font-weight-bold pb-2"><i class="fa fa-user text-ct  font-xxxmiddle mr-2" aria-hidden="true"></i>対象</h4>
                                <ul>
                                    <li>DXという言葉は聞くが<br>何かわからない企業</li>
                                    <li>DX推進人材を育てたい企業</li>
                                </ul>
                            </section>
                            <section class="border-top pt-3 pb-3 pl-2 pr-2" data-height="row2">
                                <h4 class="pl-3 font-middle font-middle font-weight-bold pb-2"><i class="fa fa-level-up text-ct  font-xxxmiddle mr-2" aria-hidden="true"></i>目指すレベル</h4>
                                <ul>
                                    <li>次世代型企業とは何かが理解できている</li>
                                    <li>変化の必要性に気付けている</li>
                                </ul>
                            </section>
                            <section class="border-top pt-3 pb-3 pl-2 pr-2" data-height="row3">
                                <h4 class="pl-3 font-middle font-middle font-weight-bold pb-2"><i class="fa fa-book text-ct  font-xxxmiddle mr-2" aria-hidden="true"></i>実施内容</h4>
                                <ul>
                                    <li>次世代型企業とは</li>
                                    <li>なぜ変化が必要なのか</li>
                                    <li>なぜ変化ができていないのか</li>
                                    <li>他社事例から考える自社の変化方法</li>
                                </ul>
                            </section>
                        </section>
                    </div>
                </div>
                <div class="col-12 col-lg-4 pb-3">
                    <div class="border h-100 rounded bg-white">
                        <section>
                            <h3 class="text-center pt-4 pb-3 text-ct font-weight-bold">ステップアップ</h3>
                            <div class="bg-lgray p-3">
                                期間：1日間<br>
                                料金：<span class=" font-xxmiddle font-weight-bold mr-1">50</span>万円／5人
                            </div>
                            <section class="pt-4 pb-3 pl-2 pr-2" data-height="row1">
                                <h4 class="pl-3 font-middle font-weight-bold pb-2"><i class="fa fa-user text-ct  font-xxxmiddle mr-2" aria-hidden="true"></i>対象</h4>
                                <ul>
                                    <li>DXを推進方法を知りたい企業</li>
                                    <li>RPAとは何かを知りたい企業</li>
                                </ul>
                            </section>
                            <section class="border-top pt-3 pb-3 pl-2 pr-2" data-height="row2">
                                <h4 class="pl-3 font-middle font-middle font-weight-bold pb-2"><i class="fa fa-level-up text-ct  font-xxxmiddle mr-2" aria-hidden="true"></i>目指すレベル</h4>
                                <ul>
                                    <li>次世代型企業への変化をどのように行うかが理解できている</li>
                                    <li>RPAとは何かが理解できている</li>
                                </ul>
                            </section>
                            <section class="border-top pt-3 pb-3 pl-2 pr-2" data-height="row3">
                                <h4 class="pl-3 font-middle font-middle font-weight-bold pb-2"><i class="fa fa-book text-ct  font-xxxmiddle mr-2" aria-hidden="true"></i>実施内容</h4>
                                <ul>
                                    <li>ITソリューションを使用したDX方法</li>
                                    <li>これまでのITソリューションと<br>RPAの違い</li>
                                    <li>RPAとは何か</li>
                                    <li>企業が抱えるRPAの課題と解決方法</li>
                                    <li>正しいRPAの使い方</li>
                                </ul>
                            </section>
                        </section>
                    </div>
                </div>
                <div class="col-12 col-lg-4 pb-3">
                    <div class="border h-100 rounded bg-white">
                        <section>
                            <h3 class="text-center pt-4 pb-3 text-ct font-weight-bold">ジャンプアップ</h3>
                            <div class="bg-lgray p-3">
                                期間：2日間<br>
                                料金：<span class=" font-xxmiddle font-weight-bold mr-1">60</span>万円／5人
                            </div>
                            <section class="pt-4 pb-3 pl-2 pr-2" data-height="row1">
                                <h4 class="pl-3 font-middle font-weight-bold pb-2"><i class="fa fa-user text-ct  font-xxxmiddle mr-2" aria-hidden="true"></i>対象</h4>
                                <ul>
                                    <li>DX推進手法を知りたい企業</li>
                                    <li>RPA導入を検討しているが人材がいない/育てたい企業</li>
                                </ul>
                            </section>
                            <section class="border-top pt-3 pb-3 pl-2 pr-2" data-height="row2">
                                <h4 class="pl-3 font-middle font-middle font-weight-bold pb-2"><i class="fa fa-level-up text-ct  font-xxxmiddle mr-2" aria-hidden="true"></i>目指すレベル</h4>
                                <ul>
                                    <li>RPAでの簡単な開発ができている</li>
                                    <li>自社でどんな業務が自動化できるかピックアップできている</li>
                                </ul>
                            </section>
                            <section class="border-top pt-3 pb-3 pl-2 pr-2" data-height="row3">
                                <h4 class="pl-3 font-middle font-middle font-weight-bold pb-2"><i class="fa fa-book text-ct  font-xxxmiddle mr-2" aria-hidden="true"></i>実施内容</h4>
                                <ul>
                                    <li>IT未経験者でもOK！<br>基礎の”キ”から学ぶRPA</li>
                                    <li>RPAを導入することの意味とは</li>
                                    <li>ハンズオンで学ぶRPAの開発方法</li>
                                </ul>
                            </section>
                        </section>
                    </div>
                </div>
            </div>
            <div class="p-3">
                <a class="btn btn-cv btn-lg w-100 w-md-450px d-block mx-auto p-3" href="#form" role="button"><i class="fa fa-chevron-circle-down mr-2" aria-hidden="true"></i>お申し込みはこちら</a>
            </div>
            <footer id="sec4"></footer>
        </div>
    </section>
    <section class="mh-400px p-md-5">
        <div class="container pb-3">
            <header class="pt-4 pb-5">
                <h2 class="font-notosans text-center text-ct text-ct font-weight-bold font-large font-md-xlarge">講師責任者のご紹介</h2>
            </header>
            <div class="row mb-3">
                <div class="col-12 col-md-4 text-center text-md-left pt-md-2" data-animation="parent">
                    <img src="/asset/img/rpa/pic_profile.png" class="w-md-80 mx-auto d-block trans-left_small trans trans-wait-5" data-animation="child">
                    <h3 class="text-center font-notosans font-weight-bold pt-4 trans-bottom_xsmall trans-wait-3 trans trans-wait-9 font-notoserif " data-animation="child">小林 太陽</h3>
                </div>
                <div class="col-12 col-md-8">
                    <section class="pb-3 line-height-2">
                        <p>某SIerにて、官公庁向けのスクラッチシステム開発に従事。ウォーターフォール型のシステム開発工程にて、全工程を経験し、業務効率化の重要性に気付く。</p>
                        <p>ITソリューションを活用し、さらなる業務をしたいという思いから、大手DXコンサルティング企業へ入社。日本企業のビジネストランスフォーメーション（BX）とデジタルトランスフォーメーション（DX）を支援したいという想いから、大手日本企業に対するDX推進、地方公営企業、製造業、流通業など幅広い業界や領域に対して、国内外問わずRPA/OCR/AIによる業務改革に従事。</p>
                        <p>日本企業の活性化のため、中小企業のDX推進が不可欠であり、中小企業に対してもDXを推進したいという想いがある一方で、大手企業への支援の中で経験したDX推進課題や失敗例から、中小企業でも同様な失敗が広がることに危機感を覚え、中小企業に対するDX推進並びに、DX人材育成に従事。</p>
                    </section>
                    <section class="border p-2">
                        <header class="bg-lgray">
                            <h3 class="pt-2 pb-2 text-center font-xxmiddle">実　績</h3>
                        </header>
                        <ul class="pt-2">
                            <li class="pb-1">物流会社へのRPA導入支援</li>
                            <li class="pb-1">地方公営企業へのRPA開発者研修講師及びRPA導入支援</li>
                            <li class="pb-1">繊維メーカー・食品会社・物流会社のRPAによるバックオフィス業務改革・ガイドライン策定支援</li>
                            <li class="pb-1">製造会社・電機メーカー等、国内/海外RPA導入及びOCR導入支援</li>
                            <li class="pb-1">某監査法人のDX構想策定及び実行支援</li>
                            <li class="pb-1">その他、プロジェクト実績多数</li>
                        </ul>
                    </section>
                </div>
            </div>
        </div>
    </section>

    <section id="form">
        <header class="pt-5 pb-5 bg-ct dec-triangle dec-cv">
            <h2 class="text-center text-white m-0 p-0 pb-1 font-xxmiddle font-md-xlarge">お申込み・お問い合わせ</h2>
        </header>
        <div class="container pt-4 pb-5">
            <form method="post" class="w-100 w-lg-75 mx-auto" action="confirm">
                <?php echo viewForm::inputText('企業名／学校名', 'inpuit1', 'required', '例）株式会社○○'); ?>
                <?php echo viewForm::inputText('業種', 'inpuit2', null, '例）広報'); ?>
                <?php echo viewForm::inputText('お名前', 'inpuit3', 'required', '例）サンプル 太郎'); ?>
                <?php echo viewForm::inputText('メールアドレス', 'inpuit4', 'required', '例）sample@docomo.ne.jp'); ?>
                <?php echo viewForm::inputText('電話番号', 'inpuit5', 'required', '例）03-5786-0135'); ?>
                <div class="mb-4">
                    <div class="row">
                        <div class="col-12 col-md-4 text-md-right mb-2">希望プラン<span class="badge badge-warning ml-2">必須</span></div>
                        <div class="col-12 col-md-8">
                            <select class="form-control" id="exampleFormControlSelect2" name="希望プラン">
                                <option disabled selected>選択してください</option>
                                <option value="相談">相談</option>
                                <option value="基礎編">基礎編</option>
                                <option value="応用編">応用編</option>
                                <option value="レビュー付演習研修">レビュー付演習研修</option>
                                <option value="カスタム研修">カスタム研修</option>
                            </select>
                        </div>
                    </div>
                </div>
                <?php echo viewForm::textarea('問い合わせ内容', 'inpuit7'); ?>
                <div class="pt-4">
                    <input name="post_type" type="hidden" value="rpa">
                    <button id="submit" type="submit" class="btn btn-cv btn-lg mx-auto d-block w-100 w-md-450px p-3"><i class="fa fa-chevron-circle-right mr-2" aria-hidden="true"></i>申し込み</button>
                </div>
            </form>
        </div>
    </section>
    <aside class="border-top bg-lgray">
        <div class="container">
            <div class="w-100 w-md-600px mx-auto">
                <div class="row align-items-center">
                    <div class="col-12 col-md-4">
                        <a href="/"><img src="/asset/img/logo_labu.png" class="img-fix p-3"></a>
                    </div>
                    <div class="col-12 col-md-8">
                        <p><?php echo config::companyName; ?></p>
                        <p><?php echo config::companyAddress; ?><br>TEL:<?php echo config::companyTel; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </aside>
</main>


<script src="/asset/js/chart.min.js"></script>
<script>
    $(function() {
        // チャートのデータ名を指定
        var myLabels = [
            "導入済み", "検討中", "未導入"
        ];

        var data1 = [{
            label: '2018年6月',
            backgroundColor: "#f7dae7",
            data: [22, 32, 46]
        }, {
            label: '2019年1月',
            backgroundColor: "#edb0cb",
            data: [32, 36, 37]
        }, {
            label: '2019年11月',
            backgroundColor: "#e283ac",
            data: [46, 31, 25]
        }];

        var data2 = [{
            label: '2018年6月',
            backgroundColor: "#f7dae7",
            data: [27, 31, 43]
        }, {
            label: '2019年1月',
            backgroundColor: "#edb0cb",
            data: [39, 33, 28]
        }, {
            label: '2019年11月',
            backgroundColor: "#e283ac",
            data: [51, 30, 19]
        }];

        var data3 = [{
            label: '2018年6月',
            backgroundColor: "#f7dae7",
            data: [22, 32, 46]
        }, {
            label: '2019年1月',
            backgroundColor: "#edb0cb",
            data: [32, 36, 37]
        }, {
            label: '2019年11月',
            backgroundColor: "#e283ac",
            data: [46, 31, 25]
        }];

        var myOptions = [{
            plugins: {
                legend: {
                    display: false
                }
            },
            scales: {
                y: {
                    title: {
                        display: true,
                        text: '%'
                    }
                }
            }
        }];
        var myScales = [{
            y: {
                title: {
                    display: true,
                    text: '%'
                }
            }
        }];

        var myPlugins = [{
            legend: {
                display: false
            }
        }];

        function setData(myDatasets) {
            return result = {
                type: 'bar',
                data: {
                    labels: myLabels,
                    datasets: myDatasets
                },
                options: {
                    plugins: {
                        legend: {
                            display: false
                        }
                    },
                    scales: {
                        y: {
                            title: {
                                display: true,
                                text: '%'
                            }
                        }
                    }
                }
            };
        }

        // チャートを表示する要素がウィンドウ内にあるかを判断する関数を定義
        function conditional(id) {
            // ウィンドウ上端の位置を取得
            var docTop = $(window).scrollTop();
            // ウィンドウ下端の位置を取得
            var docBottom = docTop + $(window).height();
            // チャート上端の位置を取得
            var elemTop = $(id).offset().top;
            // チャート下端の位置を取得
            var elemBottom = elemTop + $(id).height();
            // 「チャートを表示する要素がウィンドウ内にある場合に真となる式」を返す
            return (elemTop + 300 <= docBottom) && (docTop <= elemBottom);
        }

        function drowChart(id, config) {
            selecterId = '#' + id;
            if (conditional(selecterId)) {
                if (!$(selecterId).hasClass('active')) {
                    var ctx = document.getElementById(id);
                    var myChart = new Chart(ctx, config);
                    $(selecterId).addClass('active');
                }
            }
        }

        config1 = setData(data1);
        config2 = setData(data2);
        config3 = setData(data3);

        // スクロール時に描画アニメーションの実行を判断
        $(window).on('scroll', function() {
            drowChart('chart1', config1);
            drowChart('chart2', config2);
            drowChart('chart3', config3);
        });
        drowChart('chart1', config1);
        drowChart('chart2', config2);
        drowChart('chart3', config3);
    });
</script>

<?php
page::$js = <<<EOF
<script>
    /*KeyVisual*/
    $('[data-keyvisual]').imagesLoaded(function(){
        $('[data-keyvisual]').addClass('active');
    });
    /*mailForm*/
    $("#submit").click(function() {
        window.location.href = '#form';
    })
    /*alignHeight*/
    function alignheight(target) {
        let intHeight = 0;
        let intMaxHeight = 0;
        let heightList = [];
        /*resetHeight*/
        $('[data-height="' + target + '"]').css({
            "min-height": intHeight + "px"
        });
        /*Calculation*/
        $('[data-height="' + target + '"]').each(function(index, element) {
            intHeight = $(this).outerHeight();
            heightList.unshift(intHeight);
        });
        /*addHeight*/
        intMaxHeight = Math.max.apply(null, heightList);
        $('[data-height="' + target + '"]').css({
            "min-height": intMaxHeight + "px"
        });
    }
    let timer = false;
    $(window).resize(function() {
        if (timer !== false) {
            clearTimeout(timer);
        }
        timer = setTimeout(function() {
            if ($(window).width() >= 992) {
                alignheight("row1");
                alignheight("row2");
                alignheight("row3");
            }
        }, 500);
    });
    if ($(window).width() >= 992) {
        alignheight("row1");
        alignheight("row2");
        alignheight("row3");
    }
</script>
EOF;
?>

<?php
include '../static-tmpl/footer.php'; //フッターのテンプレート読み込み
?>