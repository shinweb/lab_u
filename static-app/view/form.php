<?php
class viewForm
{
    function inputText($name, $id, $required = null, $placeholder="")
    {
        if ($required === null) {
            $badge = '<span class="badge badge-secondary ml-2">任意</span>';
        } else {
            $badge = '<span class="badge badge-warning ml-2">必須</span>';
        }
        $body = <<<EOF
<div class="mb-3">
<div class="row">
<div class="col-12 col-lg-4 text-lg-right mb-2">{$name}{$badge}</div>
<div class="col-12 col-lg-8">
<input type="text" name="{$name}" class="form-control" placeholder="{$placeholder}" value="" id="{$id}" {$required}>
</div>
</div>
</div>
EOF;
        return $body;
    }
    function textarea($name, $id,  $required = null, $placeholder="その他、ご質問等ありましたらご記入ください")
    {
        if ($required === null) {
            $badge = '<span class="badge badge-secondary ml-2">任意</span>';
        } else {
            $badge = '<span class="badge badge-warning ml-2">必須</span>';
        }
        $body = <<<EOF
<div class="mb-3">
<div class="row">
<div class="col-12 col-lg-4 text-lg-right mb-2">{$name}{$badge}</div>
<div class="col-12 col-lg-8">
<textarea name="{$name}" class="form-control" id="{$id}" rows="10" placeholder="{$placeholder}" maxlength="1000"></textarea>
</div>
</div>
</div>
EOF;
        return $body;
    }
}
