<?php
class config
{
    const baseUrl = "http://labu.co.jp/";

    const mailAddres = "service@labu.co.jp";
    const mailAddres_test = "y-asakura@lab-unlimited.com";

    const companyName = "株式会社LAB.U";
    const companyAddress = "東京都渋谷区千駄ヶ谷3-16-1原宿プラザ401";
    const companyTel = "03-5786-0135";
}
