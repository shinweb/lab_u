<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>画像のアップロード</title>
</head>
<body>
<p><?php

const FILE_FOL = "files/";

//変数の初期化
    $check = null;
    $newfilename = null;
    $msg = null;

//元ファイル名の先頭にアップロード日時を加える

    $ext = pathinfo($_FILES["upfile"]["name"], PATHINFO_EXTENSION);
    $newfilename = date("YmdHis").mt_rand().".".$ext;

// ファイルがアップデートされたかを調べる
    if(is_uploaded_file($_FILES["upfile"]["tmp_name"])) {
        $check = 1;
    } else {
        $check = 0;
        $msg = "ファイルが選択されていません。";
    }
/*
    if ($check == 1) {
        if ($_FILES['upload']['size'] > 1000000) {
            $check = 0;
            $msg = 'ファイルサイズを小さくしてください';
        }
    }*/

//画像か調べる

    $file_pass = $_FILES["upfile"]["tmp_name"];
    
    if ($check == 1) {
        if(file_exists($file_pass) && $type = exif_imagetype($file_pass)){
            switch($type){
                //gifの場合
                case IMAGETYPE_GIF:
                break;
                //jpgの場合
                case IMAGETYPE_JPEG:
                break;
                //pngの場合
                case IMAGETYPE_PNG:
                break;
                //どれにも該当しない場合
                default:
                $msg =  "gif、jpg、png以外の画像です";
            }
        }else{
            $msg =  "画像ファイルではありません";
        }
    }

//例外処理を全てクリアしたらファイルをアップする
    if ($check == 1) {
      if (move_uploaded_file($file_pass, FILE_FOL.$newfilename)) {
        chmod(FILE_FOL. $_FILES["upfile"]["name"], 0644);
        print $newfilename. "としてファイルをアップロードしました。<BR>";
        print "<a href=".FILE_FOL.$newfilename. ">ファイルを確かめる</A><BR>";
        print "<img src=".FILE_FOL.$newfilename." width=400>";
      } else {
        print "ファイルをアップロードできません。";
      }
    } else {
    print $msg;
    }
    print "<P><a href=index.html>戻る</A>";
?></p>
</body>
</html>