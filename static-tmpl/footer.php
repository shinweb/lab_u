<footer class="bg-dark p-4 text-center">
    <small class="text-white">Copyright© LAB.U Corporation All Rights Reserved. </small>
</footer>

<div class="btn-totop is-firstview" data-firstview>
    <a href="#top" class="text-white">
        <i class="fa fa-angle-up" aria-hidden="true"></i>
    </a>
</div>

<script src="/asset/js/bootstrap.bundle.min.js"></script>
<script src="/asset/js/imagesLoaded.js"></script>
<script src="/asset/js/echo.js"></script>
<script src="/asset/js/lp.js"></script>
<?php echo page::$js; ?>

</body>

</html>