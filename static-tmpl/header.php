<!doctype html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo page::$title ?></title>
    <meta name="description" content="<?php echo page::$description; ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:type" content="website">
    <meta property="og:title" content="<?php echo page::$title; ?>">
    <meta property="og:image" content="<?php echo page::$ogimage; ?>">
    <meta property="og:url" content="<?php echo config::baseUrl; ?>">
    <meta property="og:site_name" content="<?php echo config::companyName; ?>">
    <meta property="og:description" content="<?php echo page::$description; ?>">
    <meta property="og:locale" content="ja_JP">
    <meta name="format-detection" content="telephone=no">
    <link href="/asset/css/bootstrap.min.css" rel="stylesheet" onload="this.media='all'" />
    <link href="/asset/css/font-awesome.min.css" rel="stylesheet" onload="this.media='all'" />
    <link href="/asset/css/common.css" rel="stylesheet" onload="this.media='all'" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;700&family=Noto+Sans+JP:wght@400;500;700&display=swap" rel="stylesheet">
    <?php echo page::$css; ?>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script>
        window.jQuery || document.write('<script src="/asset/js/jquery-3.3.1.min.js"><\/script>');
    </script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-XVYM7W7RM0"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'G-XVYM7W7RM0');
    </script>
</head>

<body>